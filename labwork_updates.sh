#!/bin/bash

# Anything in here should run as root

HOME=/home/kali

# Create the labwork folder
if [ ! -d $HOME/Desktop/Lab_Work ]
then
    mkdir $HOME/Desktop/Lab_Work
fi

# Ensure root doesn't own the labwork Directory...
if [ $(stat -c "%U" $HOME/Desktop/Lab_Work) = "root" ]
then
    chown kali:kali $HOME/Desktop/Lab_Work
fi

# Move/set-up the hard update script
if [ -f $HOME/Desktop/hard_update.sh ]
then
    rm $HOME/Desktop/hard_update.sh
fi
cp $HOME/Desktop/iy3840-resources/hard_update.sh $HOME/Desktop/hard_update.sh
chmod 755 $HOME/Desktop/hard_update.sh
chown kali:kali $HOME/Desktop/hard_update.sh


apt-get install -y pev

pip install capstone mmh3 scikit-learn

